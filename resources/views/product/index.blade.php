@extends('layouts.app')
@section('title', $viewData['title'])
@section('subtitle', $viewData['subtitle'])
@section('content')


     <form action="{{ route('product.index') }}" style="display: inline;" method="get">

          <select name="categorie" class="form-select" style="width: 8rem;display: inline;" id="categorie_selector">
               <option value="{{ null }}">
                    all
               </option>
               @foreach ($viewData['categories'] as $c)
                    <option value="{{ $c->get_name() }}" {{ $viewData['cat'] === $c->get_name() ? 'selected' : '' }}>
                         {{ $c->get_name() }}
                    </option>
               @endforeach

          </select>
          <button type="submit" class="btn btn-warning" style="display: inline;">Filter</button>
     </form>


     <div class="row">
          @foreach ($viewData['products'] as $product)
               <div class="col-md-4 col-lg-3 mb-2">
                    <div class="card">
                         <img src="{{ asset('/storage/' . $product->getImage()) }}" class="card-img-top img-card">
                         <div class="card-body text-center">
                              <a href="{{ route('product.show', ['id' => $product->getId()]) }}"
                                   class="btn bg-primary text-white">{{ $product->getName() }}</a>
                         </div>
                    </div>
               </div>
          @endforeach
     </div>
@endsection
