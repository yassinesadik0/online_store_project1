<?php

namespace Database\Seeders;

use App\Models\Categorie;
use App\Models\Product;
use Illuminate\Database\Seeder;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Foundation\Auth\User;

class DatabaseSeeder extends Seeder
{
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run()
	{
		
		// \App\Models\User::factory(10)->create();
		// Product::factory(10)->create();
		Categorie::factory(10)->create()->each(function ($category) {
			$category->products()->createMany(
				Product::factory(10)->make()->toArray()
			);
		});
	}
}
