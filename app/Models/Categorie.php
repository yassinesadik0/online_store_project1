<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $table = "categories";


    public static function validate($request){
        $request->validate([
            'name'=>"required",
            'description'=>"required",
        ]);
    }

    public function get_id(){
        return $this->attributes["id"];
    }
    public function set_id($new)
    {
        $this->attributes["id"] = $new;
        return $new;
    }
    public function get_name(){
        return $this->attributes["name"];
    }
    public function set_name($new)
    {
        $this->attributes["name"] = $new;
        return $new;
    }
    public function get_description(){
        return $this->attributes["description"];
    }
    public function set_description($new)
    {
        $this->attributes["description"] = $new;
        return $new;
    }
    public function products(){
        return $this->hasMany(Product::class);
    }

    public function get_products(){
        return $this->products();
    }
}
