<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{

    public function index(Request $request)
    {
        $viewData = [];
        $viewData["title"] = "Products - Online Store";
        $viewData["subtitle"] =  "List of products";
        $viewData['categories'] = Categorie::all();
	   $viewData["products"] = Product::all();
	   $viewData["cat"] = "";
	   
        $categorie = $request->query("categorie", null);
        if ($categorie){
            $cat = Categorie::where("name", "".$categorie)->first();
            $viewData["products"] = $cat->products;
            $viewData["cat"] = $cat->get_name();
        }

        return view('product.index')->with("viewData", $viewData);
    }

    public function show($id)
    {
        $viewData = [];
        $product = Product::findOrFail($id);
        $viewData["title"] = $product->getName()." - Online Store";
        $viewData["subtitle"] =  $product->getName()." - Product information";
        $viewData["product"] = $product;
        return view('product.show')->with("viewData", $viewData);
    }
}
