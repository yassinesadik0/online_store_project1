<?php

namespace App\Http\Controllers;

use App\Models\Product;
use App\Models\Order;
use App\Models\Item;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function index(Request $request)
    {
        $total = 0;
        $productsInCart = [];

        $productsInSession = json_decode($request->cookie("products"),$request->cookie("products")?true:false);
        if ($productsInSession) {
            $productsInCart = Product::findMany(array_keys($productsInSession));
            $total = Product::sumPricesByQuantities($productsInCart, $productsInSession);
        }
        // dd($productsInSession, $productsInCart);
        $viewData = [];
        $viewData["title"] = "Cart - Online Store";
        $viewData["subtitle"] =  "Shopping Cart";
        $viewData["total"] = $total;
        $viewData["products"] = $productsInCart;
        return response()->view('cart.index',["viewData" => $viewData]);
    }

    public function add(Request $request, $id)
    {
        $products = json_decode($request->cookie("products"),true);
        $products[$id] = $request->input('quantity');
        $cookie = cookie('products', json_encode($products), 360);

        return redirect()->route('cart.index')->withCookie($cookie);
    }

    public function delete(Request $request)
    {
        $request->cookie("products");
        return back()->cookie("products", null, -1);
    }

    public function purchase(Request $request)
    {
        $productsInCookie = json_decode($request->cookie('products'), true);
        if ($productsInCookie) {
            $userId = Auth::user()->getId();
            $order = new Order();
            $order->setUserId($userId);
            $order->setTotal(0);
            $order->save();

            $total = 0;
            $productsInCart = Product::findMany(array_keys($productsInCookie));
            foreach ($productsInCart as $product) {
                $quantity = $productsInCookie[$product->getId()];
                $item = new Item();
                $item->setQuantity($quantity);
                $item->setPrice($product->getPrice());
                $item->setProductId($product->getId());
                $item->setOrderId($order->getId());
                $item->save();
                $total = $total + ($product->getPrice()*$quantity);
            }
            $order->setTotal($total);
            $order->save();

            $newBalance = Auth::user()->getBalance() - $total;
            Auth::user()->setBalance($newBalance);
            Auth::user()->save();

            $Newcookie = cookie("products", null, -1);

            $viewData = [];
            $viewData["title"] = "Purchase - Online Store";
            $viewData["subtitle"] =  "Purchase Status";
            $viewData["order"] =  $order;
            return response()->view('cart.purchase',["viewData"=> $viewData])->withCookie($Newcookie);
        } else {
            return redirect()->route('cart.index');
        }
    }
}
