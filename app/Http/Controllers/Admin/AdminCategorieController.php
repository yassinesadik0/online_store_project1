<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Categorie;

class AdminCategorieController extends Controller
{
    public function index(){
        $viewData = [];
        $viewData["categories"] = Categorie::all();
        $viewData["title"] = "gestion de categorie";
        return view('admin.categorie.index')->with("viewData", $viewData);
    }
    public function store(Request $request){
        
        Categorie::validate($request);

        $cat = new Categorie();
        $cat->set_name($request->input("name"));
        $cat->set_description($request->input("description"));
        $cat->save();
        return back();
    }

    public function delete($id){
        Categorie::destroy($id);
        return back();
    }

    public function edit($id){
        $viewData = [];
        $viewData["categorie"] = Categorie::findOrFail($id);
        $viewData["title"] = "edit - categorie";
        return view('admin.categorie.edit')->with("viewData", $viewData);
    }
    public function update(Request $request,$id){
        Categorie::validate($request);
        $cat = Categorie::findOrFail($id);
        $cat->set_name($request->input("name"));
        $cat->set_description($request->input("description"));
        $cat->save();
        return redirect()->route("admin.categorie.index");
    }
}
