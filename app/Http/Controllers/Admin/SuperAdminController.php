<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;

use Illuminate\Support\Facades\DB;



use Illuminate\Support\Facades\Validator;

class SuperAdminController extends Controller
{
    //
    public function index()
    {
        $viewData = [];
        $viewData["admins"] = User::all()->where("role", "admin");
        $viewData["title"] = "Admin Page - Online Page ";
        return view("admin.superAdmin.index")->with("viewData", $viewData);
    }

   

    public function store(Request $request)
    {
        $validat = Validator::make($request->all(),  ['name' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],]);
        $user = new User();
        if($validat->fails())
        {
            return redirect()->back()->withErrors($validat)->withInput()
            ;
        }
        $user->setName($request->input("name"));
        $user->setEmail($request->input("email"));
        $user->setPassword(Hash::make($request->input("password")));
        $user->setRole("admin");
        $user->setBalance(0);

        $user->save()

        ;
        return back();
    }
    public function destroy($id)
    {
        User::destroy($id);
        
        return back();
    }
}