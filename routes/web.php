<?php

use App\Http\Controllers\Admin\AdminCategorieController;
use App\Http\Controllers\admin\SuperAdminController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'App\Http\Controllers\HomeController@index')->name("home.index");
Route::get('/about', 'App\Http\Controllers\HomeController@about')->name("home.about");
Route::get('/products/{categorie?}', 'App\Http\Controllers\ProductController@index')->where('categorie', '[A-Za-z]+')->name("product.index");
Route::get('/products/{id}', 'App\Http\Controllers\ProductController@show')->name("product.show");

Route::get('/cart', 'App\Http\Controllers\CartController@index')->name("cart.index");
Route::get('/cart/delete', 'App\Http\Controllers\CartController@delete')->name("cart.delete");
Route::post('/cart/add/{id}', 'App\Http\Controllers\CartController@add')->name("cart.add");

Route::middleware('auth')->group(function () {
    Route::get('/cart/purchase', 'App\Http\Controllers\CartController@purchase')->name("cart.purchase");
    Route::get('/my-account/orders', 'App\Http\Controllers\MyAccountController@orders')->name("myaccount.orders");
});

Route::middleware('admin')->group(function () {
    Route::get('/admin', 'App\Http\Controllers\Admin\AdminHomeController@index')->name("admin.home.index");
    Route::get('/admin/products', 'App\Http\Controllers\Admin\AdminProductController@index')->name("admin.product.index");
    Route::post('/admin/products/store', 'App\Http\Controllers\Admin\AdminProductController@store')->name("admin.product.store");
    Route::delete('/admin/products/{id}/delete', 'App\Http\Controllers\Admin\AdminProductController@delete')->name("admin.product.delete");
    Route::get('/admin/products/{id}/edit', 'App\Http\Controllers\Admin\AdminProductController@edit')->name("admin.product.edit");
    Route::put('/admin/products/{id}/update', 'App\Http\Controllers\Admin\AdminProductController@update')->name("admin.product.update");
    //categorie routes
    Route::get('/admin/categories',[AdminCategorieController::class,"index"])->name("admin.categorie.index");
    Route::post('/admin/categories/store',[AdminCategorieController::class, "store"])->name("admin.categorie.store");
    Route::delete('/admin/categories/{id}/delete',[AdminCategorieController::class, "delete"])->name("admin.categorie.delete");
    Route::get("/admin/categories/{id}/edit", [AdminCategorieController::class, "edit"])->name("admin.categorie.edit");
    Route::put("/admin/categories/{id}/update", [AdminCategorieController::class, "update"])->name("admin.categorie.update");
});

Route::middleware("superAdmin")->group(
    function (){
    Route::get("/admin/create_admins",[SuperAdminController::class,"index"])->name("superAdmin.index")
    ;
    Route::get("/admin/create_admins/{id}",[SuperAdminController::class,"destroy"])->name("superAdmin.destroy")
    ;
    Route::post("/admin/create_admins",[SuperAdminController::class,"store"])->name("superAdmin.store")
    ;
    
})
;

Auth::routes();
