<?php

namespace Tests\Unit;

// use PHPUnit\Framework\TestCase;

use App\Models\Categorie;
use Tests\TestCase;
use App\Models\User;
// use WithoutMiddleware;


class CategorieTest extends TestCase
{
	public function admin() {
		$admin = User::where("role", "admin")->first();
		if (!$admin) {
			$admin = User::factory()->create();
		}
		return $admin;
	}

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function test_category_index()
	{
		$response = $this->actingAs($this->admin())->get("/admin/categories");
		$response->assertStatus(200);
		$response->assertViewIs('admin.categorie.index');
	}

	public function test_category_store() {
		$data = [
			"name" => "testCat",
			"description" => "testDescription",
		];
		$response = $this->actingAs($this->admin())->post("/admin/categories/store", $data);
		$response->assertStatus(302);
	}

	public function test_category_update() {
		$data = [
			"name" => "testCat",
			"description" => "testDescription",
		];
		$response = $this->actingAs($this->admin())->put("/admin/categories/1/update", $data);
		$response->assertStatus(302);
	}

	public function test_category_destroy() {
		$cat = Categorie::factory()->create();
		$response = $this->actingAs($this->admin())->delete("/admin/categories/{$cat->id}/delete");
		$response->assertStatus(302);
		$this->assertDatabaseMissing("categories", ["id" => $cat->id]);
	}
}
